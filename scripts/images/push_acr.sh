#!/bin/bash
set -e

# docker login --username=1583096683@qq.com registry.cn-shenzhen.aliyuncs.com

# k8s.gcr.io/kube-apiserver:v1.22.1
# k8s.gcr.io/kube-controller-manager:v1.22.1
# k8s.gcr.io/kube-scheduler:v1.22.1
# k8s.gcr.io/kube-proxy:v1.22.1
# k8s.gcr.io/pause:3.5
# k8s.gcr.io/etcd:3.5.0-0
# k8s.gcr.io/coredns/coredns:v1.8.4
k8s_image=$1
if [[ -z "${k8s_image}" ]]; then
  echo "usage: $0 <k8s_image>"
  exit 1
fi

[[ $(echo ${k8s_image} | awk -F':' '{print $1}') = 'k8s.gcr.io/coredns/coredns' ]] && \
  acr_image=$(echo ${k8s_image} | awk -F/ '{print $3}') || \
  acr_image=$(echo ${k8s_image} | awk -F/ '{print $2}')

docker pull ${image}
docker tag ${image} registry.cn-shenzhen.aliyuncs.com/k8scat/${acr_image}
docker push registry.cn-shenzhen.aliyuncs.com/k8scat/${acr_image}
