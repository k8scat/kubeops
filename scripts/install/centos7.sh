#!/bin/bash
#
# Install kubernetes

# Install kubectl
# curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
# sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# Install kubelet kubeadm kubectl begin
# cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
# [kubernetes]
# name=Kubernetes
# baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
# enabled=1
# gpgcheck=1
# repo_gpgcheck=1
# gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
# exclude=kubelet kubeadm kubectl
# EOF

# 使用阿里云的镜像 https://developer.aliyun.com/mirror/kubernetes
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-\$basearch/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

# 将 SELinux 设置为 permissive 模式（相当于将其禁用）
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

# 初始化控制平面节点
# kubeadm init
# --apiserver-advertise-address=
# --pod-network-cidr=10.244.0.0/16
# kubeadm init
# kubeadm init --pod-network-cidr=10.244.0.0/16
kubeadm init --apiserver-advertise-address=47.119.182.100 --pod-network-cidr=10.244.0.0/16

# 在 kubeadm init 后，kubelet 会自行启动
# 设置为开机自启动并现在立刻启动
# systemctl enable kubelet
# systemctl start kubelet
# systemctl enable --now kubelet

# https://kubernetes.io/zh/docs/tasks/tools/included/optional-kubectl-configs-bash-linux/
# kubectl 启用 shell 自动补全功能
yum install -y bash-completion
# https://developer.aliyun.com/article/652961
cat <<EOF >>~/.bashrc
source <(kubectl completion bash)
export KUBECONFIG=/etc/kubernetes/admin.conf
EOF
. ~/.bash_profile

# 安装 Pod 网络附加组件
# 安装扩展（Addons） https://kubernetes.io/zh/docs/concepts/cluster-administration/addons/
# 集群网络系统 https://kubernetes.io/zh/docs/concepts/cluster-administration/networking/#how-to-implement-the-kubernetes-networking-model
# kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
# flannel 需要设置 --pod-network-cidr
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# 单机 Kubernetes 集群
# 在主节点上运行 Pod
kubectl taint nodes --all node-role.kubernetes.io/master-

# Install [metrics-server](https://github.com/kubernetes-sigs/metrics-server)
# kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl apply -f ./addons/metrics-server/v0.4.2.yaml


# Install dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
# 允许外部访问
kubectl proxy --address 0.0.0.0 --accept-hosts='^*$' &
