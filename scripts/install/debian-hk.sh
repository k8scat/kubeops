#!/bin/bash
set -e

# https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/

# 更新 apt 包索引并安装使用 Kubernetes apt 仓库所需要的包
apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl

# 下载 Google Cloud 公开签名秘钥
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# 添加 Kubernetes apt 仓库
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# 更新 apt 包索引，安装 kubelet、kubeadm 和 kubectl，并锁定其版本
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl

# Disable swap
# free -h

# 配置 cgroup 驱动程序
# https://kubernetes.io/zh/docs/tasks/administer-cluster/kubeadm/configure-cgroup-driver/
cat <<EOF >/etc/docker/daemon.json
{
  "exec-opts": [
    "native.cgroupdriver=systemd"
  ]
}
EOF
systemctl daemon-reload && systemctl restart docker

kubeadm init

# coredns 要求安装 Pod 网络插件
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml

# https://kubernetes.io/zh/docs/tasks/tools/included/optional-kubectl-configs-bash-linux/
# kubectl completion bash >/etc/bash_completion.d/kubectl
apt install -y bash-completion
cat <<EOF >>~/.bashrc
source <(kubectl completion bash)
export KUBECONFIG=/etc/kubernetes/admin.conf
EOF
. ~/.bashrc
