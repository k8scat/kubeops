#!/bin/bash
# https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#tear-down

#kubectl drain <node name> --delete-local-data --force --ignore-daemonsets

kubeadm reset

#iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X
#ipvsadm -C
