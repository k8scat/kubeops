#!/bin/bash
#
# Uninstall kubernetes and remove all resources

kubectl delete node --all
kubeadm reset
for service in kube-apiserver kube-controller-manager kubectl kubelet kube-proxy kube-scheduler; do
  systemctl stop ${service}
done
yum remove -y kubelet kubeadm kubectl kubernetes # if it's registered as a service
