# metrics-server 代替 heapster

记录一下：

1. 在尝试安装 metrics-server 时，镜像已经准备好了，而且已经下载好了，但是在 `kubectl apply -f metrics-server-v0.4.4.yaml` 后发现对应的 pods 仍然在尝试拉取镜像，
由于网络问题，这肯定是会拉取失败的，原因在于：我只在 master 节点上下载了镜像，而 pods 真正运行的节点是 node-2，导致会尝试拉取镜像。
2. 在正常安装完后，pod 会出现 CrashLoopBackOff 的状态，使用 `kubectl describe pod pod-name -n kube-system` 查看 pod 的具体描述可以发现
有一个 Unhealthy 的 event `Readiness probe failed: Get "https://192.168.20.1:4443/readyz": context deadline exceeded`，然后会主动 kill 这个容器并重建，
这里我们需要修改

https://github.com/kubernetes-sigs/metrics-server

## Prepare resource config

```bash
curl -L -o metrics-server-v0.4.4.yaml https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```

## Prepare image

Download image from ACR and re-tag

```bash
docker pull registry.cn-shenzhen.aliyuncs.com/k8scat/metrics-server:v0.4.4
docker tag registry.cn-shenzhen.aliyuncs.com/k8scat/metrics-server:v0.4.4 k8s.gcr.io/metrics-server/metrics-server:v0.4.4
docker rmi registry.cn-shenzhen.aliyuncs.com/k8scat/metrics-server:v0.4.4
```
