#!/bin/bash
#
# Stop all running containers

docker stop $(docker ps | awk 'NR != 1 {print $1}')
