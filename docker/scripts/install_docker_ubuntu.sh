#!/bin/bash
#
# Install Docker on Ubuntu
# Maintainer: k8scat@gmail.com
set -e

# Uninstall old versions
apt-get remove -y docker docker-engine docker.io containerd runc

# SET UP THE REPOSITORY
apt-get update -y
apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# INSTALL DOCKER ENGINE
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io

# Configure Docker to start on boot
systemctl enable docker.service
systemctl enable containerd.service

# Start Docker
systemctl start docker

# Verify
docker run --rm hello-world
docker rmi hello-world
