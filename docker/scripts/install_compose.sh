#!/bin/bash
#
# Install DockerCompose
# Maintainer: k8scat@gmail.com
set -e

#version=1.28.2
version=$1
curl -L "https://github.com/docker/compose/releases/download/${version}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
