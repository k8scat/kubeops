#!/bin/bash

mkdir -p /etc/docker
tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": [""]
}
EOF
systemctl daemon-reload
systemctl restart docker
