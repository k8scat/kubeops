# 节点之间配置 ssh 无密通信

## ssh key 生成

```bash
ssh-keygen -t rsa -C "k8scat@gmail.com" -f ~/.ssh/id_rsa
```

## 权限

| permission | file                           |
| ---------- | ------------------------------ |
| 0700       | ~/.ssh                         |
| 0600       | ~/.ssh/id_rsa (private key)    |
| 0644       | ~/.ssh/id_rsa.pub (public key) |
| 0644       | ~/.ssh/authorized_keys         |

## 配置 hosts

`/etc/hosts`

```hosts
192.168.0.12 node1
192.168.0.13 node2
```
