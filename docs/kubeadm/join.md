# About `kubeadm join`

## Join 前检查

```bash
kubeadm join 10.0.2.4:6443 --token ioshf8.40n8i0rjsehpigcl \
    --discovery-token-ca-cert-hash sha256:085d36848b2ee8ae9032d27a444795bc0e459f54ba043500d19d2c6fb044b065
```

### discovery-token-ca-cert-hash 用于 Node 验证 Master 身份

join 的时候，master 的 apiserver 会下发 ca.crt 到 node 节点上，然后根据 ca.crt 计算出对应的 hash，
并与 discovery-token-ca-cert-hash 进行比对，相同则表示是真正的 master

```bash
openssl x509 -in /etc/kubernetes/pki/ca.crt -noout -pubkey | \
openssl rsa -pubin -outform DER 2>/dev/null | \
sha256sum | \
cut -d' ' -f1
```

### token 用于 Master 验证 Node 身份

此部分有问题

master 的 apiserver 会获取前缀为 `bootstrap-signer-token` 的 secret，低版本的前缀是 `bootstrap-token`

```bash
kubectl get secret -n kube-system | grep bootstrap-signer-token
```
