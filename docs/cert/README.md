# Cert

Kubernetes 的证书存放目录是 /etc/kubernetes/pki

## 查看 ca.crt 证书内容

```bash
openssl x509 -in ca.crt -noout -text
```

## 验证证书的签发者

```bash
openssl verify -CAfile ca.crt apiserver-etcd-client.crt
```
