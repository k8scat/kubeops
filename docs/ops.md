# Operation

## 添加新的节点

从已有节点同步以下文件：

/etc/systemd/system/kubelet.service.d/10-kubeadm.conf
/etc/kubernetes/pki/ca.crt
/etc/kubernetes/kubelet.conf

## 移除节点

```bash
kubectl drain node node-1 # 隔离节点、释放节点上的 pods
kubectl delete node node-1 # 将 node-1 从集群的 node 列表中删除

# 重启 kubelet，node-1 便进入没有加入任何集群的状态
# 或者 kubectl reset
```

## 节点扩容

```bash
kubectl cordon node-1 # Mark node as unschedulable
kubectl uncordon node-1 # Mark node as schedulable
```

## label 管理

包括 node、pod、service 对象

### 查看 label

使用 `--show-labels` 查看

```bash
kubectl get nodes --show-labels
kubectl get pods --all-namespaces --show-labels
kubectl get services --show-labels # kubectl get svc --show-labels
```

## 添加 label

使用 `kubectl label` 命令

```bash
kubectl label pod PodName key=value
```

### 删除 label

只需要在 key 后面加上减号 -

```bash
kubectl label pod PodName key-
```

### 更新 label

使用 `--overwrite` 参数

```bash
kubectl label pod PodName key=new-value --overwrite
```

## namespace 管理

### 增删 namespace

```bash
kubectl create namespace foo # 创建名为 foo 的 namespace

kubectl create -f nginx-deployment.yaml -n foo # 在 foo 这个 namespace 下创建 nginx-deployment

kubectl delete namespace foo # 同时会删除 namespace 下的 pods
```

### 通过 context 简化不同 namespace 下的运维操作

通过定义 context 来省略 `-n` 参数

```bash
kubectl config get-contexts # 获取当前的 context 列表

kubectl config set-context foo-ctx --namespace=foo --cluster=kubernetes --user=kubernetes-admin # 创建 namespace 为 foo 的 context

kubectl config use-context foo-ctx # 使用 foo-ctx 作为当前的 context

```

## pod 管理

### 增删 pod

```bash
kubectl create -f pod.yaml

kubectl delete -f pod.yaml

kubectl delete pod PodName
```

### 查看 pod

```bash
kubectl get pods # 查看当前 namespace 下的 pods

kubectl describe pod PodName

kubectl get pod PodName -o yaml # 获取 pod 的 yaml
```

### 更新 pod

```bash
# 更新 yaml 文件，然后 kubectl apply -f pod.yaml

kubectl edit pod PodName
```

## service 管理

### 增删 service

```bash
kubectl create -f nginx-svc.yaml

kubectl delete -f nginx-svc.yaml

kubectl delete service ServiceName
```

### 查看 service

```bash
kubectl get services

kubectl describe service ServiceName

kubectl get service ServiceName -o yaml
```

### 更新 service

```bash
# 更新 yaml 文件，然后 kubectl apply -f nginx-service.yaml

kubectl edit service nginx-service
```

## 计算资源管理

### 术语

- 资源请求 Resource Requests
- 资源限制 Resource Limits
- CPU 资源单位 cpu单元
- Memory 资源单位 bytes

## 日志查看

### 平台组件日志

- node 组件日志，包括 kubelet（systemd） 和 kube-proxy

kubelet 日志 `journalctl -u kubelet -f`
kube-proxy 日志 `cd /var/log/containers`

- master 组件日志，包括 kube-apiserver-master、kube-controller-manager-master 和 kube-scheduler-master，都是以 Static Pod 的形式运行

/var/log/containers

- addons 组件日志，dns，cni，dashboard

/var/log/containers

### 工作负载

同样也可以在 /var/log/containers 下查看日志，使用 `kubectl logs pod PodName -f -n NamespaceName` 查看多副本的 Pod 日志

使用 kubectl exec 进入 Pod 查看

使用 docker 命令查看（启动失败的 Pod）

### 查看事件 event （存储在 etcd 中，1小时过期，event 导出到第三方）

kubectl get event 获取 namespace 下的 event 列表

也可以使用 kubectl describe pod PodName 查看 pod 的 event

## Troubleshooting

### 集群自身

apiserver schedular contoller-manager
kubelet
addons: dns/cni

### 工作负载 Pod

pod 状态

- pending 资源不足、端口占用（使用 service 对外开放端口）
- Waiting containerCreating 镜像拉取失败、CNI 网络错误、镜像自身问题
- ImagePullBackOff 镜像拉取问题，手动拉取 docker pull(私有镜像仓 dockerregistry secret)
- CrashLoopBackOff 异常退出，restart 大于0，docker logs查看容器日志，或者 kubectl describe pod，健康检查失败
- Error 启动过程中发生错误，manifest 配置问题，依赖的资源不存在，请求的资源超过限制，违反了集群的安全策略，容器无权操作集群内的资源，开启 RBAC 后需要设置角色绑定
- Terminating Unknown

### Service

Service 状态

service Endpoints missing：label 匹配导致，service 与 pod 的 label 是否匹配

service 网络流量不转发
