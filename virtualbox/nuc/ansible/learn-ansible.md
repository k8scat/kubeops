# Ansible 学习笔记

[Documentation](https://docs.ansible.com/ansible/latest/index.html)

## 安装

先使用脚本 `goworker/python/install_python.sh` 安装 python3，然后直接使用 pip 安装 ansible

```bash
# 使用了 root 用户，所以这里没有再用 --user
# python 安装的目录是 /usr/local/python3.8.12，ansible 将会被放在 /usr/local/python3.8.12/bin 下面
pip install ansible==4.6.0
```

## 配置文件

配置文件主要包括 ansible.cfg 和 hosts 两个文件

### ansible.cfg 配置

读取配置文件的优先级：

1. 环境变量 ANSIBLE_CONFIG
2. 当前目录下的 ./ansible.cfg
3. HOME 目录下的 ~/.ansible.cfg
4. /etc/ansible/ansible.cfg

使用 `ansible --version` 可以查看当前使用的配置文件路径

### hosts 主机资产配置

主要有三种配置方式：

- 用户名 + 用户密码
- 用户名 + 私钥
- 主机别名 + 用户名 + 私钥（host 相同的场景下）

```toml
[group_name]
192.168.1.110:22 ansible_ssh_user=root ansible_ssh_pass='123456'
192.168.1.111:22 ansible_ssh_user=root ansible_ssh_private_key_file=/root/.ssh/id_rsa
test1 ansible_ssh_host=192.168.1.112 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_private_key_file=/root/.ssh/id_rsa
```

## 执行模式

### adhoc 模式

ad-hoc 模式就是直接使用 `ansible` 命令在主机上进行批量执行命令

```bash
> ansible all -a 'date'
node2 | CHANGED | rc=0 >>
Sat Oct  9 16:19:59 EDT 2021
node3 | CHANGED | rc=0 >>
Sat Oct  9 16:20:00 EDT 2021
node1 | CHANGED | rc=0 >>
Sat Oct  9 16:20:00 EDT 2021
ops | CHANGED | rc=0 >>
Sat Oct  9 16:20:00 EDT 2021
```

格式是：ansible <host-pattern> [options]，其中 host-pattern 用于匹配主机名或主机组名（好像匹配不了主机 host，可以匹配主机别名）

```bash
ansible 192.168.1.* -a 'ls /tmp'
ansible group1 -a 'ls /tmp' # 可以匹配 group1
ansible host1 -a 'ls /tmp' # 可以匹配 host1
```

ad-hoc 常用模块：

- command 默认模块

```bash
# -f FORKS, --forks FORKS specify number of parallel processes to use (default=5)
ansible -a '/sbin/reboot' -f 10 servers
```

- shell 执行shell命令

```bash
ansible -m shell -a 'echo $HOSTNAME' servers
```

> command vs. shell

```bash
# 下面命令输出的将是 $HOSTNAME，而不是变量的值
ansible -a 'echo $HOSTNAME' servers

# 无法使用管道，下面的命令将会出现错误而中断
ansible -a 'ps aux | wc -l' servers
```

- file transfer 文件传输

```bash
ansible -m copy -a "src=/etc/hosts dest=/etc/hosts" servers
```

- managing packages 管理软件包

```bash
# state: present latest removed
ansible -m yum -a "name=nginx state=present" servers
```

- user and groups 用户和组

```bash
ansible -m user -a "name=jeson password=123456" servers
```

- deploying 部署模块

```bash
ansible -m git -a "repo=https://gitee.com/k8scat/gigrator.git dest=/opt/gigrator version=master" servers
```

- managing services 服务管理

```bash
ansible -m service -a "name=nginx state=started" servers
```

- background operations 后台运行

```bash
ansible -B 3600 -a "/usr/bin/running_operation --do-stuff" servers
```

- gathering facts 搜集系统信息

```bash
ansible -m setup -a "filter=ansible_distribution" servers
```

### playbooks 模式

- [Playbooks 入门](https://ansible-tran.readthedocs.io/en/latest/docs/playbooks_intro.html)
- [ansible-examples](https://github.com/ansible/ansible-examples)

```bash
# 使用格式
ansible-playbook [options] <playbooks>

# 指定 inventory 主机资源文件
ansible-playbook -i /etc/ansible/hosts touch_file.yml

# 查看帮助
ansible-playbook -h
```

> yaml 使用 `---` 分隔多个文件，也可以使用 `...` 表示文件结尾

#### 使用变量

主要有三种方式：

- playbook 文件中使用 `vars` 进行定义

```yml
vars:
  touch_file: test.txt
```

- 作为 ansible-playbook 命令的参数

```bash
ansible-playbook --extra-vars "touch_file=test.txt" touch_file.yml
```

- 使用 register 从其他 task 中获取内容

```yml
---
- hosts: node1
  remote_user: root
  vars:
    touch_file: test.txt
  tasks:
    - name: get date
      command: date
      register: date_output
    - name: touch file
      shell: "touch /tmp/{{ touch_file }}"
    - name: echo date
      shell: echo {{ date_output.stdout }} >> /tmp/{{ touch_file }}
```

#### 条件控制

##### when 条件控制

在 CentOS7 或 Debian7 中创建文件，
且文件名使用了两个变量，分别是 ansible_distribution 和 ansible_distribution_major_version

```yml
---
- hosts: node1,node2
  remote_user: root
  tasks:
  - name: touch file
    command: touch /tmp/{{ ansible_distribution }}_{{ ansible_distribution_major_version }}.txt
    when: |
      (ansible_distribution == "CentOS" and ansible_distribution_major_version == "7") or
      (ansible_distribution == "Debian" and ansible_distribution_major_version == "7")
```

##### 循环语句

| 循环类型           | 关键字             |
| ------------------ | ------------------ |
| 标准循环           | with_items         |
| 嵌套循环           | with_nested        |
| 遍历字典           | with_dict          |
| 并行遍历列表       | with_together      |
| 遍历列表和索引     | with_indexed_items |
| 遍历文件列表的内容 | with_file          |
| 遍历目录文件       | with_fileglob      |
| 重试循环           | until              |
| 查找第一个匹配文件 | with_first_found   |
| 随机选择           | with_random_choice |
| 在序列中循环       | with_sequence      |

案例：https://www.imooc.com/article/22753

##### 条件循环语句结合使用

```yml
---
- hosts: node1
  remote_user: root
  tasks:
  - name: when and with_dict
    debug: msg="{{ item.key }} is the winner"
    with_dict: {'jerry': {'english': 60, chinese: '20'}, 'tom': {'english': 20, 'chinese': 30}}
    when: item.value.english >= 60
```

#### 异常处理

- 忽略错误

```yml
---
- hosts: node1
  remote_user: root
  tasks:
  - name: ignore errors
    command: /bin/false
    ignore_errors: true
  - debug: msg='hello world'
```

- 自定义错误

当 node1 主机上的进程数大于 3 时，抛出自定义错误，这里使用了 `processes_count.stdout|int` 将字符串转换为数字并进行判断，参考 [ansible playbook: check linux process count](https://stackoverflow.com/questions/41256309/ansible-playbook-check-linux-process-count)

```yml
---
- hosts: node1
  tasks:
    - name: count processes
      shell: |
        # https://xan.manning.io/2019/03/21/ansible-lint-rule-306.html
        set -o pipefail
        ps aux | wc -l
      register: processes_count
      # https://stackoverflow.com/questions/41256309/ansible-playbook-check-linux-process-count
      failed_when: processes_count.stdout|int < 3
      changed_when: false
    - name: debug
      debug: msg='{{ processes_count }}'
    - name: touch file
      file: path=/tmp/test.txt state=touch mode=0700 owner=root group=root
```

- 改变 changed 状态

```yml
---
- hosts: node1
  remote_user: root
  tasks:
  - name: ignore errors
    command: /bin/true
    changed_when: false
  - debug: msg='hello world'
```

### 标签

可以设置标签的对象有：单个 task、include 对象、roles 对象

- 单个 task

```yml
# tags.yml
---
- hosts: node1
  tasks:
  - name: create file1
    shell: touch /tmp/file1
    tags:
    - tag1
    - tag2
  - name: create file2
    shell: touch /tmp/file2
    tags:
    - tag1
```

执行上面的 playbook：

```bash
# 使用 -t 或 --tags 进行指定需要执行的标签
# 仅执行 tag2 标记的 task
ansible-playbook -t tag2 tags.yml

# 执行 tag1 和 tag2 标记的 task
ansible-playbook -t tag1,tag2 tags.yml

# 使用 --skip-tags 进行指定忽略的标签
# 忽略 tag1 和 tag2 标记的 task
ansible-playbook --skip-tags tag1,tag2 tags.yml 
```

### notify 和 handler

handler 只有在 task 中声明了 notify 且 task 的运行结果状态为 changed 时才会被执行（主动设置 `changed_when: true`，即 task 的运行结果状态永远都是 changed）。

如果设置了 `changed_when: false`，那么 handler 将永远不会被执行。

下面的 playbook 的 handler 并不会被执行，因为 err task 失败了，导致 handler 虽然被触发了，但没有被执行
（可以使用 `ignore_errors: true` 让 err task 忽略错误）。

参考：[ansible中notify和handler使用“跳坑”记](https://www.codenong.com/js9da04a554060/)

```yml
---
- hosts: node1
  tasks:
    - name: notify
      command: /bin/true
      notify:
        - hello world
      # changed_when: true
    - name: err task
      command: /bin/false
      # ignore_errors: true
  handlers:
    - name: hello world
      shell: echo 'hello world' > /tmp/notify_handler1.txt
```

### 重用 playbooks

[Re-using Ansible artifacts](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse.html)
