# nuc nodes

## 网络

### CentOS 7 网络配置

/etc/sysconfig/network-scripts/ifcfg-enp0s3

```ini
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no

BOOTPROTO=static # dhcp

DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s3

UUID=79ecba66-a0ac-4c34-bd32-99d71a31bfd7 # uuidgen

DEVICE=enp0s3

ONBOOT=yes # 开机启动

IPADDR=192.168.8.143
NETMASK=255.255.255.0
GATEWAY=192.168.8.1 # netstat -rn

DNS1=223.5.5.5
DNS2=223.6.6.6
DNS3=114.114.114.114
DNS4=8.8.8.8
```

重启网络服务：`systemctl restart network`

### IP、Subnet mask、Gateway、Broadcast address（广播地址）

私有网段地址：

- A类 10.0.0.0～10.255.255.255 10/8
- B类 172.16.0.0～172.31.255.255 172.16/12
- C类 192.168.0.0～192.168.255.255 192.168/16

#### 子网掩码

子网掩码(subnet mask)又叫网络掩码、地址掩码、子网络遮罩，它是一种用来指明一个IP地址的哪些位标识的是主机所在的子网，以及哪些位标识的是主机的位掩码。

子网掩码不能单独存在，它必须结合IP地址一起使用。子网掩码只有一个作用，就是将某个IP地址划分成网络地址和主机地址两部分。

子网掩码是一个32位地址，用于屏蔽IP地址的一部分以区别网络标识和主机标识，并说明该IP地址是在局域网上，还是在远程网上。

子网掩码 —— 屏蔽一个 IP 地址的网络部分的 “全1” 比特模式。
对于A类地址来说，默认的子网掩码是 255.0.0.0；
对于B类地址来说，默认的子网掩码是 255.255.0.0；
对于C类地址来说，默认的子网掩码是 255.255.255.0。

- 通过子网掩码，就可以判断两个IP在不在一个局域网内部。

- 子网掩码可以看出有多少位是网络号，有多少位是主机号：255.255.255.0 二进制是：11111111 11111111 11111111 00000000

网络号24位，即全是1 主机号8位，即全是0

129.168.1.1/24 这个 24 就是告诉我们网络号是 24 位，也就相当于告诉我们子网掩码是：11111111 11111111 11111111 00000000，即：255.255.255.0

172.16.10.33/27 中的 27 也就是说子网掩码是 255.255.255.224，即 27 个全 1，11111111 11111111 11111111 11100000

#### 网关

网关(Gateway)又称网间连接器、协议转换器。默认网关在网络层上以实现网络互连，是最复杂的网络互连设备，仅用于两个高层协议不同的网络互连。网关的结构也和路由器类似，不同的是互连层。网关既可以用于广域网互连，也可以用于局域网互连

网关实质上是一个网络通向其他网络的IP地址。

比如有网络A和网络B，网络A的IP地址范围为 192.168.1.1~192.168.1.254，子网掩码为 255.255.255.0；
网络B的IP地址范围为 192.168.2.1~192.168.2.254，子网掩码为 255.255.255.0。

在没有路由器的情况下，两个网络之间是不能进行 TCP/IP 通信的，即使是两个网络连接在同一台交换机（或集线器）上，TCP/IP 协议也会根据子网掩码（255.255.255.0）判定两个网络中的主机处在不同的网络里。

而要实现这两个网络之间的通信，则必须通过网关。如果网络A中的主机发现数据包的目的主机不在本地网络中，就把数据包转发给它自己的网关，再由网关转发给网络B的网关，网络B的网关再转发给网络B的某个主机。

所以说，只有设置好网关的IP地址，TCP/IP 协议才能实现不同网络之间的相互通信。那么这个IP地址是哪台机器的IP地址呢？网关的IP地址是具有路由功能的设备的IP地址，具有路由功能的设备有路由器、启用了路由协议的服务器（实质上相当于一台路由器）、代理服务器（也相当于一台路由器）。

#### 广播地址

广播地址(Broadcast Address)是专门用于同时向网络中所有工作站进行发送的一个地址。

在使用 TCP/IP 协议的网络中，主机标识段 host ID 为全 1 的 IP 地址为广播地址，广播的分组传送给 host ID 段所涉及的所有计算机。
例如，对于 10.1.1.0（255.255.255.0 ）网段，其广播地址为 10.1.1.255（255 即为2 进制的11111111 ），当发出一个目的地址为 10.1.1.255 的分组（封包）时，它将被分发给该网段上的所有计算机。

#### 使用 ip 命令

查看网卡、Gateway、IP、Gateway

```bash
ip route

default via 10.0.3.2 dev enp0s8 proto dhcp metric 101 
default via 192.168.8.1 dev enp0s3 proto static metric 102 
default via 10.0.2.1 dev enp0s9 proto static metric 103 
10.0.0.0/8 dev enp0s9 proto kernel scope link src 10.0.2.4 metric 103 
10.0.3.0/24 dev enp0s8 proto kernel scope link src 10.0.3.15 metric 101 
172.17.0.0/16 dev docker0 proto kernel scope link src 172.17.0.1 
192.168.2.0/24 dev enp0s3 proto kernel scope link src 192.168.2.98 metric 102 
192.168.8.1 dev enp0s3 proto static scope link metric 102
```

上面的内容可以看出有三个网卡，分别是：

- enp0s3：桥接网卡（这个网卡连接的是主机网络，而主机网络会变化，所以不应该设置 BOOTPROTO=static）
- enp0s8：网络地址转换（NAT），针对单个固定的虚拟机
- enp0s9：NAT 网络，在全局设定中添加 NAT 网络，可以用于多个虚拟机

#### 查看网络

```bash
ifconfig -a

ip a

ip route
```

### 查看 Gateway

```bash
netstat -rn

Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         10.0.3.2        0.0.0.0         UG        0 0          0 enp0s8
0.0.0.0         192.168.8.1     0.0.0.0         UG        0 0          0 enp0s3
0.0.0.0         10.0.2.1        0.0.0.0         UG        0 0          0 enp0s9
10.0.0.0        0.0.0.0         255.0.0.0       U         0 0          0 enp0s9
10.0.3.0        0.0.0.0         255.255.255.0   U         0 0          0 enp0s8
172.17.0.0      0.0.0.0         255.255.0.0     U         0 0          0 docker0
192.168.2.0     0.0.0.0         255.255.255.0   U         0 0          0 enp0s3
192.168.8.1     0.0.0.0         255.255.255.255 UH        0 0          0 enp0s3
```

## 主机名

```bash
# 设置主机名
hostnamectl set-hostname node1
```
